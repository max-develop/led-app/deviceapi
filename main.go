package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func handle() {
	r := mux.NewRouter()
	r.HandleFunc("/", Health).Methods("GET")
	r.HandleFunc("/device-api/create-device", CreateDevice).Methods("POST")
	r.HandleFunc("/device-api/update-device", UpdateDevice).Methods("PUT")
	r.HandleFunc("/device-api/update-rgb", UpdateRGB).Methods("PUT")
	log.Fatal(http.ListenAndServe(":80", r))
}

func main() {
	handle()
}
