package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

//Health is a simple Healtpoint
func Health(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("/ Hit!"))
}

//CreateDevice send the new Device to the redis-crud
func CreateDevice(w http.ResponseWriter, r *http.Request) {
	dev := NewEmptyDevice()
	HTTPclient := &http.Client{}
	//read device info from client
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println("[ERROR] Can not parse client data: ", err)
		w.WriteHeader( http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not parse client data"))
		return
	}
	err = json.Unmarshal(body, &dev)
	if err != nil {
		log.Println("[ERROR] Can not parse client data: ", err)
		w.WriteHeader( http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not parse client data"))
		return
	}
	//convert device struct to JSON
	devJSON, err := json.Marshal(dev)
	if err != nil {
		log.Println("[ERROR] Can not convert struct to JSON: ", err)
		w.WriteHeader( http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not convert struct to JSON"))
		return
	}

	serviceName := "redis-rest:80"
	updateURL := fmt.Sprintf("http://%s/api/create-device", serviceName)
	//create request to agent
	req, err := http.NewRequest("POST", updateURL, strings.NewReader(string(devJSON)))
	if err != nil {
		log.Println("[ERROR] Can not create request to Redis-API: ", err)
		w.WriteHeader( http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not create request to Redis-API"))
		return
	}
	//send request
	_, err = HTTPclient.Do(req)
	if err != nil {
		log.Println("[ERROR] Can not connect to Redis-API: ", err)
		w.WriteHeader( http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not connect to Redis-API"))
		return
	}

}

//UpdateDevice sends a notification to the agent if something has changed
func UpdateDevice(w http.ResponseWriter, r *http.Request) {
	dev := NewEmptyDevice()
	HTTPclient := &http.Client{}
	//read device info from client
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println("[ERROR] Can not parse client data", err)
		return
	}
	err = json.Unmarshal(body, &dev)
	if err != nil {
		log.Println("[ERROR] Can not parse client data: ", err)
		w.WriteHeader( http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not parse client data"))
		return
	}

	//convert device struct to JSON
	devJSON, err := json.Marshal(dev)
	if err != nil {
		log.Println("[ERROR] Can not convert struct to JSON: ", err)
		w.WriteHeader( http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not convert struct to JSON"))
		return
	}

	serviceName := "redis-rest:80"
	updateURL := fmt.Sprintf("http://%s/api/update-device", serviceName)
	//create request to agent
	req, err := http.NewRequest("PUT", updateURL, strings.NewReader(string(devJSON)))
	if err != nil {
		log.Println("[ERROR] Can not create request to Redis-API: ", err)
		w.WriteHeader( http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not create request to Redis-API"))
		return
	}
	//send request
	_, err = HTTPclient.Do(req)
	if err != nil {
		log.Println("[ERROR] Can not connect to Redis-API: ", err)
		w.WriteHeader( http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not connect to Redis-API"))
		return
	}


	serviceName = "device-blinkt:80"
	updateURL = fmt.Sprintf("http://%s/update", serviceName)
	//create request to agent
	req, err = http.NewRequest("PUT", updateURL, strings.NewReader(string(devJSON)))
	if err != nil {
		log.Println("[ERROR] Can not create request to Redis-API: ", err)
		w.WriteHeader( http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not create request to Redis-API"))
		return
	}
	//send request
	_, err = HTTPclient.Do(req)
	if err != nil {
		log.Println("[ERROR] Can not connect to device: ", err)
		w.WriteHeader( http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not connect to device"))
		return
	}

}


//UpdateRGB sends a notification to the agent if the RGB values has changed
func UpdateRGB(w http.ResponseWriter, r *http.Request) {
	dev := NewEmptyDevice()
	HTTPclient := &http.Client{}
	//read device info from client
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println("Error parsing client data", err)
	}
	err = json.Unmarshal(body, &dev)
	if err != nil {
		log.Println("[ERROR] Can not parse client data: ", err)
		w.WriteHeader( http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not parse client data"))
		return
	}

	//convert device struct to JSON
	devJSON, err := json.Marshal(dev)
	if err != nil {
		log.Println("[ERROR] Can not convert struct to JSON: ", err)
		w.WriteHeader( http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not convert struct to JSON"))
		return
	}

	serviceName := "redis-rest:80"
	updateURL := fmt.Sprintf("http://%s/api/update-device", serviceName)
	//create request to agent
	req, err := http.NewRequest("PUT", updateURL, strings.NewReader(string(devJSON)))
	if err != nil {
		log.Println("[ERROR] Can not create request to Redis-API: ", err)
		w.WriteHeader( http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not create request to Redis-API"))
		return
	}
	//send request
	_, err = HTTPclient.Do(req)
	if err != nil {
		log.Println("[ERROR] Can not connect to Redis-API: ", err)
		w.WriteHeader( http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not connect to Redis-API"))
		return
	}

	serviceName = "device-blinkt:80"
	updateURL = fmt.Sprintf("http://%s/update-RGB", serviceName)
	//create request to agent
	req, err = http.NewRequest("PUT", updateURL, strings.NewReader(string(devJSON)))
	if err != nil {
		log.Println("[ERROR] Can not create request to device: ", err)
		w.WriteHeader( http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not create request to device"))
		return
	}
	//send request
	_, err = HTTPclient.Do(req)
	if err != nil {
		log.Println("[ERROR] Can not connect to device: ", err)
		w.WriteHeader( http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not connect to device"))
		return
	}

}
